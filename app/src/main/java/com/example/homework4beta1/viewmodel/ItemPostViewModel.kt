package com.example.homework4beta1.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.homework4beta1.repository.Repository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject
import kotlinx.coroutines.launch

@HiltViewModel
class ItemPostViewModel @Inject constructor(private val repository: Repository) : ViewModel()  {

    val posts = repository.posts

    init {
        getItemsPosts()
    }

    private fun getItemsPosts() {
        viewModelScope.launch {
            try {
                repository.getPhotos()
            } catch (e: Exception) {
            }
        }
    }
 }