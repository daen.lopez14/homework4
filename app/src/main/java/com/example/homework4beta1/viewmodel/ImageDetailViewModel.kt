package com.example.homework4beta1.viewmodel

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import com.example.homework4beta1.data.model.PhotosAPI
import com.example.homework4beta1.repository.Repository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class ImageDetailViewModel@Inject constructor(private val repository: Repository, savedStateHandle: SavedStateHandle) : ViewModel() {

    val post: PhotosAPI = savedStateHandle.get<PhotosAPI>("photo")!!

}

