package com.example.homework4beta1.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.example.homework4beta1.data.local.posts.PhotoDao
import com.example.homework4beta1.data.local.posts.asDomainModel
import com.example.homework4beta1.data.model.PhotosAPI
import com.example.homework4beta1.data.model.asDatabaseModel
import com.example.homework4beta1.data.remote.WebService
import javax.inject.Inject
import javax.inject.Singleton
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext


@Singleton
class Repository @Inject constructor(
    private val remoteDataSource: WebService,
    private val photoDao: PhotoDao,
) {
    val posts: LiveData<List<PhotosAPI>> = Transformations.map(photoDao.getDataSet()) {
        it.asDomainModel()
    }

    suspend fun getPhotos() {
        withContext(Dispatchers.IO) {
            val posts = remoteDataSource.getPosts().subList(0, 25)
            photoDao.insertDataSet(posts.asDatabaseModel())
        }
    }
}