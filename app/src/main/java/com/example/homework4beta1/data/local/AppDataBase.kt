package com.example.homework4beta1.data.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.homework4beta1.data.local.posts.PhotoDao
import com.example.homework4beta1.data.local.posts.PhotoPost


@Database(entities = arrayOf(PhotoPost::class), version = 1, exportSchema = false)

abstract class AppDatabase : RoomDatabase() {

    abstract fun itemDao(): PhotoDao

    companion object {
        @Volatile
        private var INSTANCE: AppDatabase? = null

        fun getDatabase(context: Context): AppDatabase {
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    AppDatabase::class.java,
                    "app_database"
                ).build()
                INSTANCE = instance
                // return instance
                instance
            }
        }
    }
}