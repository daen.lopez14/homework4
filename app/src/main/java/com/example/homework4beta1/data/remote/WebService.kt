package com.example.homework4beta1.data.remote

import com.example.homework4beta1.data.model.PhotosAPI
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET

private const val BASE_URL =
    "https://jsonplaceholder.typicode.com/"

private val retrofit = Retrofit.Builder().baseUrl(BASE_URL)
    .addConverterFactory(GsonConverterFactory.create())
    .build()

interface WebService {

    @GET("photos")
    suspend fun getPosts(): List<PhotosAPI>

}

object RetrofitClient {
    val webService: WebService by lazy {
        retrofit.create(WebService::class.java)
    }
}