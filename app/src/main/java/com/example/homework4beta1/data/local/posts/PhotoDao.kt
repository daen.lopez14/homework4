package com.example.homework4beta1.data.local.posts

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

/**
 * dao handles a list of posts from database
 */

@Dao
interface PhotoDao {

    //devuelve una lista de ItemPost ya mapeados
    @androidx.room.Query("SELECT * FROM PhotoPost")
    fun getDataSet(): LiveData<List<PhotoPost>>

    //inserta lista de ItemPost ya mapeados a la base de datos
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertDataSet(posts: List<PhotoPost>)

}