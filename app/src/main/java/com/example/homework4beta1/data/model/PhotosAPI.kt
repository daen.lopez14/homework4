package com.example.homework4beta1.data.model

import android.os.Parcelable
import com.example.homework4beta1.data.local.posts.PhotoPost
import com.squareup.moshi.Json
import kotlinx.parcelize.Parcelize

@Parcelize
data class PhotosAPI(
    @Json(name="albumId") val albumId: Int,
    @Json(name="id")val id: Int,
    @Json(name="title")val title: String,
    @Json(name="url")val url: String,
    @Json(name="thumbnailUrl")val thumbnailUrl: String,
) : Parcelable

/**
 * Convert Network results to database objects through .asDataBaseModel, mapping ItemPost to dataBaseModel
 */

fun List<PhotosAPI>.asDatabaseModel(): List<PhotoPost> {
    return map {
        PhotoPost(
            albumId = it.albumId,
            id = it.id,
            title = it.title,
            url = it.url,
            thumbnailUrl = it.thumbnailUrl
        )
    }
}