package com.example.homework4beta1.data.local.posts

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.example.homework4beta1.data.model.PhotosAPI
import com.example.homework4beta1.data.model.asDatabaseModel
import com.example.homework4beta1.data.remote.WebService
import javax.inject.Inject
import javax.inject.Singleton
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

@Singleton
class ItemDataSource @Inject constructor(
    private val photoDao: PhotoDao,
    private val remoteDataSource: WebService
) {

    val posts: LiveData<List<PhotosAPI>> = Transformations.map(photoDao.getDataSet()) {
        it.asDomainModel()
    }

    suspend fun refreshPosts() {
        withContext(Dispatchers.IO) {
            val posts = remoteDataSource.getPosts()
            photoDao.insertDataSet(posts.asDatabaseModel())
        }
    }
}
