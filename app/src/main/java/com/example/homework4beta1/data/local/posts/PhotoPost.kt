package com.example.homework4beta1.data.local.posts

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.homework4beta1.data.model.PhotosAPI

@Entity
data class PhotoPost(
    @PrimaryKey val id: Int,
    @NonNull @ColumnInfo(name = "albumId") val albumId: Int,
    @NonNull @ColumnInfo(name = "title") val title: String,
    @NonNull @ColumnInfo(name = "url") val url: String,
    @NonNull @ColumnInfo(name = "thumbnailUrl") val thumbnailUrl: String
)

/**
 * Map Database ItemPost to domain entities
 */

fun List<PhotoPost>.asDomainModel(): List<PhotosAPI> {
    return map {
        PhotosAPI(
            albumId = it.albumId,
            id = it.id,
            title = it.title,
            url = it.url,
            thumbnailUrl = it.thumbnailUrl
        )
    }
}
