package com.example.homework4beta1.ui.itempostsui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.homework4beta1.databinding.FragmentItemlistBinding
import com.example.homework4beta1.viewmodel.ItemPostViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ItemListFragment : Fragment() {

    private val viewModel: ItemPostViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        val binding = FragmentItemlistBinding.inflate(inflater)

        binding.viewModel = viewModel

        binding.lifecycleOwner = this

        binding.recyclerViewPosts.adapter = ItemsPostAdapter()

        binding.recyclerViewPosts.layoutManager = LinearLayoutManager(requireContext())
        binding.recyclerViewPosts.setHasFixedSize(true)

        return binding.root

    }
}