package com.example.homework4beta1.ui.itempostsui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.homework4beta1.data.model.PhotosAPI
import com.example.homework4beta1.databinding.ItemsPostBinding

class ItemsPostAdapter() : ListAdapter<PhotosAPI, ItemsPostAdapter.PostsViewHolder>(DiffCallback) {

    class PostsViewHolder(private var binding: ItemsPostBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(post: PhotosAPI) {
            binding.photo = post
            binding.executePendingBindings()
        }
        init {
            binding.setClickListener {
                binding.photo?.let { post ->
                    navigateToDetail(post, it)
                }
            }
        }
        private fun navigateToDetail(post: PhotosAPI, view: View) {
            val direction =
                ItemListFragmentDirections.actionItemListFragmentToImageDetailFragment(post)
            view.findNavController().navigate(direction)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostsViewHolder {
        return PostsViewHolder(
            ItemsPostBinding.inflate(LayoutInflater.from(parent.context))
        )
    }

    override fun onBindViewHolder(holder: PostsViewHolder, position: Int) {
        val currentItem = getItem(position)
        holder.bind(currentItem)
    }

    companion object DiffCallback : DiffUtil.ItemCallback<PhotosAPI>() {
        override fun areItemsTheSame(oldItem: PhotosAPI, newItem: PhotosAPI): Boolean {
            return oldItem.id == newItem.id
        }
        override fun areContentsTheSame(oldItem: PhotosAPI, newItem: PhotosAPI): Boolean {
            return oldItem.title == newItem.title
        }
    }
}