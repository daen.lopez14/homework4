package com.example.homework4beta1.utils

import com.example.homework4beta1.data.remote.RetrofitClient
import com.example.homework4beta1.data.remote.WebService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class NetworkModule {

    @Singleton
    @Provides
    fun providePostService(): WebService {
        return RetrofitClient.webService
    }
}