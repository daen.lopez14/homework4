package com.example.homework4beta1.utils

import android.content.Context
import com.example.homework4beta1.data.local.AppDatabase
import com.example.homework4beta1.data.local.posts.PhotoDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class DataModule {

    @Singleton
    @Provides
    fun provideAppDatabase(@ApplicationContext context: Context): AppDatabase {
        return AppDatabase.getDatabase(context)
    }

    @Provides
    fun providePostDao(appDatabase: AppDatabase): PhotoDao {
        return appDatabase.itemDao()
    }
}